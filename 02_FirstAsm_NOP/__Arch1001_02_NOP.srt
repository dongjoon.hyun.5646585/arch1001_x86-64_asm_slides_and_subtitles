0
00:00:00,080 --> 00:00:04,000
all right well i want to get you to your

1
00:00:01,920 --> 00:00:06,000
first assembly instruction asap

2
00:00:04,000 --> 00:00:07,200
and so here we go first assembly

3
00:00:06,000 --> 00:00:10,480
instruction is

4
00:00:07,200 --> 00:00:12,400
NOP or a no-op no operation

5
00:00:10,480 --> 00:00:14,240
doesn't take any registers doesn't take

6
00:00:12,400 --> 00:00:17,359
any values it doesn't do anything

7
00:00:14,240 --> 00:00:18,400
it's just no operation so frequently you

8
00:00:17,359 --> 00:00:20,320
will see the NOP

9
00:00:18,400 --> 00:00:21,520
assembly instruction added to for

10
00:00:20,320 --> 00:00:23,920
instance pad or

11
00:00:21,520 --> 00:00:24,640
align bytes so you might see it between

12
00:00:23,920 --> 00:00:26,800
functions

13
00:00:24,640 --> 00:00:28,320
inside of some compiled assembly

14
00:00:26,800 --> 00:00:30,000
specifically because intel has

15
00:00:28,320 --> 00:00:31,679
optimization guidance that has to do

16
00:00:30,000 --> 00:00:33,360
with like oh the functions should start

17
00:00:31,679 --> 00:00:35,760
on you know a 16 byte aligned address or

18
00:00:33,360 --> 00:00:38,000
whatever 8 byte aligned address

19
00:00:35,760 --> 00:00:39,280
and so the compiler will stick in some

20
00:00:38,000 --> 00:00:40,960
no op instructions

21
00:00:39,280 --> 00:00:42,879
in the middle they're not actually going

22
00:00:40,960 --> 00:00:44,079
to be executed they're just there to

23
00:00:42,879 --> 00:00:45,680
pattern align things

24
00:00:44,079 --> 00:00:47,760
in some other architectures sometimes

25
00:00:45,680 --> 00:00:48,239
this might be used to just delay time so

26
00:00:47,760 --> 00:00:50,640
it just

27
00:00:48,239 --> 00:00:52,079
takes up some cycle time and does

28
00:00:50,640 --> 00:00:54,079
nothing

29
00:00:52,079 --> 00:00:56,000
also it's worth knowing that no op

30
00:00:54,079 --> 00:00:58,399
actually has some value to

31
00:00:56,000 --> 00:01:00,079
attackers when they try to make their

32
00:00:58,399 --> 00:01:01,600
exploits more reliable

33
00:01:00,079 --> 00:01:03,680
but that's going to be covered in a

34
00:01:01,600 --> 00:01:05,760
different class

35
00:01:03,680 --> 00:01:07,680
all right so some late breaking news and

36
00:01:05,760 --> 00:01:09,600
late breaking to me anyways i went you

37
00:01:07,680 --> 00:01:11,600
know many many years before i knew this

38
00:01:09,600 --> 00:01:13,840
fact so i wanted to give it to you early

39
00:01:11,600 --> 00:01:14,720
a little bit of interesting trivia that

40
00:01:13,840 --> 00:01:17,040
the one byte

41
00:01:14,720 --> 00:01:18,240
no op instruction is an alias mnemonic

42
00:01:17,040 --> 00:01:22,080
for the exchange

43
00:01:18,240 --> 00:01:25,759
eax eax instruction or rax rax now

44
00:01:22,080 --> 00:01:27,680
in our 64-bit extended architecture

45
00:01:25,759 --> 00:01:29,280
so i never actually looked in the

46
00:01:27,680 --> 00:01:31,200
assembly manual for no op because

47
00:01:29,280 --> 00:01:32,799
there's no point right and i just told

48
00:01:31,200 --> 00:01:34,240
you what a no-op does once you know that

49
00:01:32,799 --> 00:01:35,680
a no-up does nothing

50
00:01:34,240 --> 00:01:37,759
you know that it's really just there for

51
00:01:35,680 --> 00:01:39,360
padding or time delay there's no real

52
00:01:37,759 --> 00:01:41,680
point to look in the assembly manual

53
00:01:39,360 --> 00:01:43,680
but i had an attendee of a previous

54
00:01:41,680 --> 00:01:44,799
class point this out to me that he had

55
00:01:43,680 --> 00:01:47,200
looked in the

56
00:01:44,799 --> 00:01:49,200
in assembly manual and that there was

57
00:01:47,200 --> 00:01:51,680
this interesting bit of trivia

58
00:01:49,200 --> 00:01:53,200
so exchange is not actually part of this

59
00:01:51,680 --> 00:01:54,159
class not officially something you're

60
00:01:53,200 --> 00:01:56,240
going to learn but

61
00:01:54,159 --> 00:01:58,320
it's pretty simple it just exchanges the

62
00:01:56,240 --> 00:02:00,079
value between two registers

63
00:01:58,320 --> 00:02:01,840
so whatever's in the register on this

64
00:02:00,079 --> 00:02:02,479
side exchanges with that exchange with

65
00:02:01,840 --> 00:02:04,079
that

66
00:02:02,479 --> 00:02:05,759
so you know it's pretty easy to

67
00:02:04,079 --> 00:02:08,560
understand what exchange does

68
00:02:05,759 --> 00:02:09,280
and if you're exchanging eax with eax or

69
00:02:08,560 --> 00:02:11,120
rax with

70
00:02:09,280 --> 00:02:13,360
rax you exchange your register with

71
00:02:11,120 --> 00:02:13,920
itself clearly that does absolutely

72
00:02:13,360 --> 00:02:15,920
nothing

73
00:02:13,920 --> 00:02:17,599
and just a brief note about the NOP

74
00:02:15,920 --> 00:02:18,000
instruction usually when people talk

75
00:02:17,599 --> 00:02:21,200
about

76
00:02:18,000 --> 00:02:22,000
NOP they're referring to 0x90 the

77
00:02:21,200 --> 00:02:24,800
canonical

78
00:02:22,000 --> 00:02:25,520
version of NOP is a single byte 

79
00:02:24,800 --> 00:02:28,160
0x90.

80
00:02:25,520 --> 00:02:29,760
but in reality intel actually specifies

81
00:02:28,160 --> 00:02:32,000
that there can be different forms of

82
00:02:29,760 --> 00:02:33,599
NOP between one and nine bytes long

83
00:02:32,000 --> 00:02:35,519
and a little bit in this class we're

84
00:02:33,599 --> 00:02:37,040
actually going to see an example of a

85
00:02:35,519 --> 00:02:39,200
multibyte NOP that

86
00:02:37,040 --> 00:02:40,640
appears in one of the hello world

87
00:02:39,200 --> 00:02:41,920
examples

88
00:02:40,640 --> 00:02:43,599
so there you go we're going to have this

89
00:02:41,920 --> 00:02:45,200
accumulating count of assembly

90
00:02:43,599 --> 00:02:47,200
instructions you know as you go along

91
00:02:45,200 --> 00:02:50,720
and then boom you know NOP

92
00:02:47,200 --> 00:02:52,720
and ta-dah you now know six percent of

93
00:02:50,720 --> 00:02:56,319
all assembly instructions found in a web

94
00:02:52,720 --> 00:02:56,319
browser pretty cool

